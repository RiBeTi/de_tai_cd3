function check() {
    var name = localStorage.getItem('name');
    if (name == null) {
        document.getElementById('address').innerHTML = '(Không có thông tin)';
    } else {
        document.getElementById('name').innerHTML = name;
    }
    var email = localStorage.getItem('email');
    if (email == null) {
        document.getElementById('address').innerHTML = '(Không có thông tin)';
    } else {
        document.getElementById('email').innerHTML = email;
    }
    var phone = localStorage.getItem('phone');
    if (phone == null) {
        document.getElementById('address').innerHTML = '(Không có thông tin)';
    } else {
        document.getElementById('phone').innerHTML = phone;
    }
    var address = localStorage.getItem('address');
    if (address == null) {
        document.getElementById('address').innerHTML = '(Không có thông tin)';
    } else {
        document.getElementById('address').innerHTML = address;
    }
}
check();
function updateName() {
    var name = document.getElementById('name_change').value;
    localStorage.setItem('name', name);
}
function updatePhone() {
    var phone = document.getElementById('phone_change').value;
    localStorage.setItem('phone', phone);
}
function updateEmail() {
    var email = document.getElementById('email_change').value;
    localStorage.setItem('email', email);
}
function updateAddress() {
    var address = document.getElementById('address_change').value;
    localStorage.setItem('address', address);
}