function formValidate() {

    // check name
    var name = document.getElementById("name").value;
    var errorName = document.getElementById("errorName");
    var regexName = /^[^\d+]*[\d+]{0}[^\d+]*$/;
    if (!regexName.test(name)) {
        errorName.innerHTML = "Họ và tên không đươc chứa chữ số!";
    } else if (name == null || name == "") {
        errorName.innerHTML = "Điền đầy đủ họ tên!"
    } else {
        errorName.innerHTML = '';
        
    }

    // check phone
    var phone = document.getElementById("phone").value;
    var errorPhone = document.getElementById("errorPhone");
    var regexPhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if ((phone == null || phone == "")) {
        errorPhone.innerHTML = "Điền đầy đủ SĐT!"
    } else if (!regexPhone.test(phone)) {
        errorPhone.innerHTML = "SĐT không đươc chứa kí tự!";
    } else {
        errorPhone.innerHTML = '';
    }

    // check email
    var email = document.getElementById("email").value;
    var errorEmail = document.getElementById("errorEmail");
    var regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email == null || email == "") {
        errorEmail.innerHTML = "Điền đầy đủ email!"
    } else if (!regexEmail.test(email)) {
        errorEmail.innerHTML = "Email nhập vào không hợp lệ";
        email = '';
    } else {
        errorEmail.innerHTML = '';
        
    }

    // check password
    var password = document.getElementById("pass").value;
    var errorPassword = document.getElementById("errorPass");
    var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    if (password == null || password == "") {
        errorPassword.innerHTML = "Điền đầy đủ mật khẩu!"
    } else if (!regexPassword.test(password)) {
        errorPassword.innerHTML = "Mật khẩu nhập vào không hợp lệ";
    } else {
        errorPassword.innerHTML = '';
        
    }

    // check confirm Pass
    var confirmPass = document.getElementById("confirmPass").value;
    var errorConPass = document.getElementById("errorConPass");

    if (confirmPass == null || confirmPass == "") {
        errorConPass.innerHTML = "Vui lòng xác nhận mật khẩu!";
       
    } else if (confirmPass != password) {
        errorConPass.innerHTML = "Mật khẩu không trùng khớp!";
        
    } else {
        errorConPass.innerHTML = "";
    }

    if(name && phone && email && password == confirmPass) {
        alert("Đăng kí thành công!");
        window.location.pathname="/De_Tai_CD3/HTML/login.html"
    }

    return false;
}
function check() {
    var name = document.getElementById("name").value;
    localStorage.setItem('name', name);
    var phone = document.getElementById('phone').value;
    localStorage.setItem('phone', phone);
    var email = document.getElementById('email').value;
    localStorage.setItem('email', email);
    var pass = document.getElementById('confirmPass').value;
    localStorage.setItem('password', pass);
}